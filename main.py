### Main file to run code for the support zip summarizer

from xml.etree import ElementTree as ET

greeting = """
Welcome to the support zip summarizer! I show you the most important
information from a Confluence support zip in a single location.

This is an early version of the summarizer created on July 9, 2017.
For latest version, go to:
https://bitbucket.org/zlaughton/support-zip-summarizer
"""

print(greeting)
print('Path to support zip folder (drag and drop folder here):')
sz_path = input()
sz_path = sz_path.strip()
application_xml = sz_path + "/" + "application-properties/application.xml"

def get_values(tree):
    """Takes xml tree and returns most important
    values in a dictionary"""
    values = {}
    values_to_get = {           # Dictionary of important values
        'Generated date': 'application-information/system-date',
        'Generated time': 'application-information/system-time',
        'Confluence version': 'application-information/version',
        'Base URL': 'application-information/base-url',
        'Server ID': 'system-properties/server.id',
        'SEN': 'sen',
        'Java version': 'java-runtime-environment/java.runtime.version',
        'Application server': 'java-runtime-environment/application-server',
        'OS name': 'operating-system/os-name',
        'OS version': 'operating-system/os-version',
        'Database name': 'database-information/database-name',
        'Database version': 'database-information/database-version',
        'Database driver': 'database-information/driver-version'
        }

    for key, value in values_to_get.items():    # Put values in dictionary
        values[key] = tree.find(value).text

    return values

def main():
    with open(application_xml, "r") as open_file:
        tree = ET.parse(open_file)      # Create tree from file
        values = get_values(tree)

        print('\n')
        for key, value in values.items():
            print(key + ":", value)
        print('\n')

main()
